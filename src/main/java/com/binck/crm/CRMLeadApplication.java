package com.binck.crm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CRMLeadApplication {

	public static void main(String[] args) {
		SpringApplication.run(CRMLeadApplication.class, args);
	}
}
