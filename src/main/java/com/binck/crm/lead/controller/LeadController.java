package com.binck.crm.lead.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.binck.crm.lead.model.LeadRequestModel;
import com.binck.crm.lead.model.LeadResponseModel;

@RestController
@RequestMapping("/leads")
public class LeadController {

	/**
	 * @param portfolioModel
	 * @return this is a post request for adding new leads into the system. here we
	 *         are not adding any service or dao layer. it is just for demonstrate
	 *         purpose
	 */
	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<LeadResponseModel> addLead(@RequestBody LeadRequestModel leadRequestModel) {
		LeadResponseModel leadResponseModel = new LeadResponseModel();
		return new ResponseEntity<LeadResponseModel>(leadResponseModel, HttpStatus.OK);
	}

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity getLeads() {
		return new ResponseEntity(HttpStatus.OK);
	}

	@PutMapping(path= "{/leadId}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity updateLead(@PathVariable String leadId, @RequestBody LeadRequestModel leadRequestModel) {
		return new ResponseEntity(HttpStatus.OK);
	}

	@PatchMapping(path= "{/leadId}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity partialUpdateLead(@PathVariable String leadId, @RequestBody LeadRequestModel leadRequestModel) {
		return new ResponseEntity(HttpStatus.OK);
	}

	@DeleteMapping(path= "{/leadId}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity deleteLead(@PathVariable String leadId) {
		return new ResponseEntity(HttpStatus.OK);
	}

}
