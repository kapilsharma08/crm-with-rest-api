# CRM with Rest API

To run this application run CRMLeadApplication.java as a java program and spring boot will start with it's inbuilt tomcat server on port 8080.
Below are the urls for http requets:

GET: http://localhost:8080/leads
POST: http://localhost:8080/leads
For above post request we need to pass LeadRequestModel as Json or Xml in request body
DELETE: http://localhost:8080/leads/1
PUT: http://localhost:8080/leads/1
For above put request we need to pass LeadRequestModel as Json or Xml in request body
PATCH: http://localhost:8080/leads/1
For above patch request we need to pass LeadRequestModel as Json or Xml in request body


